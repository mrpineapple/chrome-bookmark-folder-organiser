require 'rubygems'
require 'json'

counter = 1


def folderize(path_so_far,element)
    for subelement in element["children"]:
        if subelement["type"] == "folder":
            Dir.mkdir(File.join(path_so_far + "/" + element["name"] , subelement["name"])) #=> 0
            folderize(path_so_far+ "/" + element["name"],subelement)
        else
            url = subelement["url"]?subelement["url"]:"Broken"
            File.open(path_so_far+ "/" + element["name"] + "/" + subelement["name"].tr('/','.'), 'w') {|f| f.write(subelement["date_added"] + "," + subelement["id"] + ","  + url) }
        end
    end
end

def rChildren(location)
    a = Dir.entries(location).select {|entry| File.directory? File.join(location) and !(entry =='.' || entry == '..' || entry == '.git') }
    children = []
    for folder in a do
        if FileTest.directory?(location + "/" +folder)
            children << {"children" => rChildren(location+"/"+folder),
                         "id" => "nope",
                         "name" => folder,
                         "date_added"=> "13013694907779962",
                         "date_modified"=> "13013694907779962",
                         "type"=> "folder"
                         }
        else
            rFile = File.read(location +"/" + folder)
            sFile = rFile.split(",")

            children << {"type"=> "url",
                        "id"=> sFile[1],
                        "name" => folder,
                        "date_added"=> sFile[0],
                        "url"=> sFile[2]
                        }
        end
    end 
    #files also
    return children
end

def getFolderJson()
    a = Dir.entries(Dir.getwd).select {|entry| File.directory? File.join(Dir.getwd,entry) and !(entry =='.' || entry == '..' || entry == '.git') }
    children = []

    for folder in a do
        children << rChildren(Dir.getwd+"/"+folder)
    end 
    return {'bookmark_bar' => {"children" => children}, "other" => "nope","synched" => "nope"}

end

if ARGV[0] == "-f" and ARGV[1]:
    #Folderize
    #cant use ~ for some reason 
    word = File.read("/home/" + ARGV[1] + "/.config/google-chrome/Default/Bookmarks")
    out = JSON.parse(word)


    for element in out["roots"]["bookmark_bar"]["children"] do
        Dir.mkdir(File.join(Dir.getwd, element["name"])) #=> 0
        folderize(Dir.getwd,element)
    end
elsif ARGV[0] == "-b":
    #bookmarkize
    begin
      file = File.open(Dir.getwd + "/" + "Bookmarks", "w")

      roots =  {'checksum' => '', "roots" => getFolderJson(),"version" => 1}

      file.write roots.to_json

    rescue IOError => e
      #some error occur, dir not writable etc.
    ensure
      file.close unless file == nil
    end
else
    puts "-f [username] or -b"
end